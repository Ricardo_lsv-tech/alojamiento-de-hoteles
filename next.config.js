/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    remotePatterns: [
      {
        hostname: 'p1.pxfuel.com'
      },
      {
        hostname: 'q-xx.bstatic.com'
      },
      {
        hostname: 'imgcy.trivago.com'
      },
      {
        hostname: 'encrypted-tbn0.gstatic.com'
      },
      {
        hostname: 'mlir1wulbk5z.i.optimole.com'
      },
      {
        hostname: 'upload.wikimedia.org'
      },
      {
        hostname: 'www.quierohotel.com'
      },
      {
        hostname: 'losparraleshotel.com'
      },
      {
        hostname: 'static.motelnowapp.com'
      }
    ]
  }
}

module.exports = nextConfig
