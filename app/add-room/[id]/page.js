'use client'

import './add-room.css'

import React, { useState } from 'react'

import { useForm } from 'react-hook-form'
import { useGetHotels } from '@/app/context/context-hotel'
import { useRouter } from 'next/navigation'
import { uuid } from 'uuidv4'

function AddRoom({ params }) {
  const { setListHotels, listHotels } = useGetHotels()
  const { id } = params
  const [images, setImages] = useState()
  const router = useRouter()
  const hotel = listHotels.find((hotel) => hotel.id === id)
  const {
    register,
    reset,
    handleSubmit,
    formState: { errors }
  } = useForm()

  function handleImage(e) {
    setImages(e.target.files[0])
  }

  const onSubmit = handleSubmit((values) => {
    values.id = uuid()
    values.image = images
    hotel.rooms = [...hotel.rooms, values]
    reset()
    router.push(`/details-hotels/${id}`)
  })
  return (
    <div className='main-add-hotel'>
      <h1>Agregar Habtacion</h1>
      <form onSubmit={onSubmit}>
        <div className='form-group'>
          <label htmlFor='room_type'>Tipo de Cuarto:</label>
          <select
            {...register('room_type', {
              required: {
                value: true,
                message: 'Tipo de cuarto es requerido'
              }
            })}
            id='room_type'
          >
            <option value='default'>Seleccione una opcion...</option>
            <option value='familiar'>Familiar</option>
            <option value='matrimonial'>Matrimonial</option>
            <option value='sencillo'>Sencillo</option>
          </select>
          {errors?.room_type && <p className='errors'>{errors.room_type.message}</p>}
        </div>
        <div className='form-group'>
          <label htmlFor='price_base'>Precio base:</label>
          <input
            type='text'
            {...register('price_base', {
              required: {
                value: true,
                message: 'Precio base es requerido'
              }
            })}
            id='price_base'
            placeholder='EX: 180000'
          />
          {errors?.price_base && <p className='errors'>{errors.price_base.message}</p>}
        </div>
        <div className='form-group'>
          <label htmlFor='inpuesto'>Impuesto:</label>
          <input type='text' {...register('impuesto', { required: false })} id='inpuesto' placeholder='EX: 19' />
        </div>
        <div className='form-group'>
          <label htmlFor='image'>Imagen del cuarto:</label>
          <input
            type='file'
            {...register('image', {
              required: {
                value: true,
                message: 'Por favor seleccione una foto'
              },
              onChange: handleImage
            })}
            id='image'
          />

          {errors?.image && <p className='errors'>{errors.image.message}</p>}
        </div>
        <div className='form-group btn'>
          <input type='submit' value='Guardar' />
          <button onClick={() => router.push(`/details-hotels/${id}`)}>Volver</button>
        </div>
      </form>
    </div>
  )
}

export default AddRoom
