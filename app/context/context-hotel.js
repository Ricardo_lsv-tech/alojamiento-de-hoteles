'use client'

import { Children, createContext, useContext, useState } from 'react'

import list_hotel from '../mocks/hotels.json'

const ContextHotel = createContext()

export default function HotelProvider({ children }) {
  const [listHotels, setListHotels] = useState(list_hotel)
  return (
    <ContextHotel.Provider
      value={{
        listHotels,
        setListHotels
      }}
    >
      {children}
    </ContextHotel.Provider>
  )
}

export const useGetHotels = () => {
  const { listHotels, setListHotels } = useContext(ContextHotel)
  return { listHotels, setListHotels }
}
