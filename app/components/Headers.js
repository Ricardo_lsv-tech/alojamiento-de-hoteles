import Link from 'next/link'
import React from 'react'

function Headers() {
  return (
    <nav className='navigation'>
        <h2>TRAVELS AGENCY</h2>
        <ul className='menu-headers'>
            <li>
                <Link href="/">Sign In</Link>
            </li>
        </ul>
      
    </nav>
  )
}

export default Headers
