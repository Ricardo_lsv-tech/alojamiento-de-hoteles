import Link from 'next/link'
import React from 'react'

function MenuHotel() {
  return (
    <nav className='menu-hotel'>
      <ul className='menu-hotel-items'>
        <li>
          <Link href='/add-hotel'>Agregar Hotel </Link>
        </li>
        <li>
          <Link href='/'>Ver Reservas</Link>
        </li>
      </ul>
    </nav>
  )
}

export default MenuHotel
