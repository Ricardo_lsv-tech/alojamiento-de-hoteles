'use client'

import InfoHotel from './InfoHotel'
import Link from 'next/link'
import React from 'react'
import { useGetHotels } from '../context/context-hotel'

function ListHotels() {
  const { listHotels } = useGetHotels()
  return (
    <div className='list-hotels'>
      {listHotels.map((hotel) => (
        <Link href={`/details-hotels/${hotel.id}`} key={hotel.id}>
          <InfoHotel hotel={hotel} />
        </Link>
      ))}
    </div>
  )
}

export default ListHotels
