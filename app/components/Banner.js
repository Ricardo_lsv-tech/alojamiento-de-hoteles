'use client'

import React, { useState } from 'react'

import Image from 'next/image'

function Banner() {
  return (
    <div className='banner'>
      <div className='title-banner'>
        <h2>DISFRUTE DE LOS MEJORES HOTELES</h2>
        <p>Viaja, Disfruta y Vive.</p>
      </div>
      <Image src='https://p1.pxfuel.com/preview/78/838/443/cartagena-colombia-caribbean-architecture.jpg' alt='imagenes de caratgena' width={930} height={400} />
    </div>
  )
}

export default Banner
