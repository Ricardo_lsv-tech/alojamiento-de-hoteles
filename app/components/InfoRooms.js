import Image from 'next/image'
import React from 'react'

function InfoRooms({ room }) {
  let price = Number(room.price_base) * (1 + Number(room.impuesto / 100))
  const room_price = price.toLocaleString('en')

  return (
    <section className='rooms-card'>
      <div className='img-container'>
        <Image src={typeof room.image === 'string' ? room.image : window.URL.createObjectURL(room.image)} alt='cuartos de hoteles' width={200} height={200} />
      </div>
      <p>{room.room_type.toUpperCase()}</p>
      <p>Precio Cop: $ {room_price}</p>
      <div className='button-group'>
        <button className='button-reserva'>Reservar</button>
      </div>
    </section>
  )
}

export default InfoRooms
