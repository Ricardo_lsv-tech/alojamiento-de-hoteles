import Image from 'next/image'
import React from 'react'

function InfoHotel({ hotel }) {
  return (
    <section className='hotel-card'>
      <div className='img-container'>
        <Image src={typeof hotel.image_url === 'string' ? hotel.image_url : window.URL.createObjectURL(hotel.image_url)} alt={hotel.name} width={300} height={300} />
      </div>
      <h3>{hotel.name}</h3>
      <p>{hotel.address}</p>
    </section>
  )
}

export default InfoHotel
