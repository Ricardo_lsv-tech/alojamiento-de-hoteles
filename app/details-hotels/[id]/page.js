'use client'

import './details.css'

import Image from 'next/image'
import InfoRooms from '@/app/components/InfoRooms'
import Link from 'next/link'
import React from 'react'
import { useGetHotels } from '@/app/context/context-hotel'

function DetailsHotels({ params }) {
  const { id } = params
  const { listHotels } = useGetHotels()
  const hotel = listHotels.find((hotel) => hotel.id === id)
  return (
    <div>
      <section className='details-hotel'>
        <div className='container-one'>
          <Image src={typeof hotel.image_url === 'string' ? hotel.image_url : URL.createObjectURL(hotel.image_url)} alt={hotel.name} width={400} height={400} />
        </div>
        <div className='container-two'>
          <h2>{hotel.name.toUpperCase()}</h2>
          <h4>{hotel.address}</h4>
          <div className='btn-hotels'>
            <button className='btn-one'>Deshabilitar</button>
            <button className='btn-two'>Editar</button>
          </div>
        </div>
      </section>
      <div className='add-room'>
        <Link href={`/add-room/${hotel.id}`}>Agregar Habitacion</Link>
      </div>
      <section className='list-rooms'>{hotel.rooms.length > 0 ? hotel.rooms.map((room) => <InfoRooms key={room.id} room={room} />) : <p>No hay Cuartos registrados ....</p>}</section>
      <div className='button-main'>
        <Link href='/'>Volver</Link>
      </div>
    </div>
  )
}

export default DetailsHotels
