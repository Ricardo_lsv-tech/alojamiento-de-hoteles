import Banner from './components/Banner'
import ListHotels from './components/ListHotels'
import MenuHotel from './components/MenuHotel'

export default function Home() {
  return (
    <main>
      <Banner />
      <MenuHotel />
      <ListHotels />
    </main>
  )
}
