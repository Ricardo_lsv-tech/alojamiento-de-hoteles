'use client'

import './add-hotel.css'

import React, { useState } from 'react'

import { useForm } from 'react-hook-form'
import { useGetHotels } from '../context/context-hotel'
import { useRouter } from 'next/navigation'
import { uuid } from 'uuidv4'

function AddHotel() {
  const router = useRouter()
  const { setListHotels, listHotels } = useGetHotels()
  const [images, setImages] = useState()
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors }
  } = useForm()

  function handleImages(e) {
    setImages(e.target.files[0])
  }

  const onSubmit = handleSubmit((values) => {
    values.id = uuid()
    values.image_url = images
    values.rooms = []
    setListHotels([...listHotels, values])
    reset()
    router.push('/')
  })
  return (
    <div className='main-add-hotel'>
      <h1>Agregar Hotel</h1>
      <form onSubmit={onSubmit}>
        <div className='form-group'>
          <label htmlFor='name'>Nombre:</label>
          <input
            type='text'
            {...register('name', {
              required: {
                value: true,
                message: 'Nombre del hotel es requerido'
              }
            })}
            id='name'
            placeholder='Ex: Hotel las margaras...'
          />
          {errors.name && <p style={{ color: 'red' }}>{errors.name.message}</p>}
        </div>
        <div className='form-group'>
          <label htmlFor='address'>Dirección:</label>
          <input
            type='text'
            {...register('address', {
              required: {
                value: true,
                message: 'Dirección del hotel es requerida'
              }
            })}
            id='address'
            placeholder='Ex: Cartegena Bocagrande...'
          />
          {errors.address && <p style={{ color: 'red' }}>{errors.address.message}</p>}
        </div>
        <div className='form-group'>
          <label htmlFor='image_url'>Imagen:</label>
          <input
            type='file'
            {...register('image_url', {
              required: {
                value: true,
                message: 'Foto del hotel es requerida'
              },
              onChange: handleImages
            })}
            id='image_url'
          />
          {errors.image_url && <p style={{ color: 'red' }}>{errors.image_url.message}</p>}
        </div>
        <div className='form-group btn'>
          <input type='submit' value='Guardar' />
          <button onClick={() => router.push('/')}>Volver</button>
        </div>
      </form>
    </div>
  )
}

export default AddHotel
